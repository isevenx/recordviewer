#ifndef RECORDVIEWER_H
#define RECORDVIEWER_H

#include <QThread>
#include <QMainWindow>

#include <fstream>
#include <opencv2/opencv.hpp>

#include "grabber.h"
#include "writer.h"

using namespace cv;

namespace Ui {
class recordViewer;
}

class recordViewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit recordViewer(QWidget *parent = 0);
    ~recordViewer();
signals:
    void changeFrame(Mat frame);

public slots:
    void startRecord();
    void stopRecord();
    void getFrame(Mat frame);

private:    
    Ui::recordViewer *ui;    
    QThread *grabThread;
    QThread *writeThread;
    Grabber *grabber;
    Writer *writer;

    Mat Frame;

public slots:
    void closeEvent(QCloseEvent*);
};

#endif // RECORDVIEWER_H
