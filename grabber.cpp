#include "grabber.h"

#include "QTimer"

Grabber::Grabber(QObject *parent) : QObject(parent)
{
    FPS = 30;
    pause = 0;
    ifStop = false;
    cap = new VideoCapture;
    cap->open(0);
    srand(time(NULL));
}

Grabber::~Grabber()
{

}

void Grabber::startGrab(QTime atTime)
{
    QTime mTime = QTime::currentTime();
    QTimer::singleShot(mTime.msecsTo(atTime),this,SLOT(grab()));
}

void Grabber::grab()
{
    if(ifStop) return;
    Mat frame,tFrame;
    QTime pTime;
    int readtime;

    pTime.start();

//    FPS = 25 / (rand()%10 + 4);

//    for(int i = 4; i > FPS/4; i--)
//        cap->grab();

    if(cap->read(frame)){
        emit sendFrame(frame);
    }
    readtime = pTime.elapsed();
    if(readtime > 1000/FPS)
        readtime = -(1000/FPS-1);

    qDebug() << readtime;
    QTimer::singleShot(1000/FPS-readtime,this,SLOT(grab()));
}

void Grabber::stopGrab()
{
    //    cap->release();
    //    delete cap;
    //    ifStop = false;
}
