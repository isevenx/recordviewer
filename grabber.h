#ifndef GRABBER_H
#define GRABBER_H

#include <QObject>
#include <QDebug>
#include <QThread>
#include <QTime>

#include <fstream>
#include <cstdlib>
#include <opencv2/opencv.hpp>

using namespace cv;

class Grabber : public QObject
{
    Q_OBJECT
public:
    explicit Grabber(QObject *parent = 0);
    ~Grabber();

     VideoCapture *cap;
     bool ifStop;
     bool ifExit;
signals:
     void sendFrame(Mat frame);

public slots:
     void startGrab(QTime atTime);
     void stopGrab();
private slots:

     void grab();
private:
     int pause, FPS;
signals:

public slots:
};

#endif // GRABBER_H
