#include "recordviewer.h"
#include <QApplication>

#include <opencv2/opencv.hpp>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qRegisterMetaType< Mat >("Mat");
    cv::namedWindow("Video Viewer",1);
    cv::namedWindow("Matrix Viewer",3);
    recordViewer w;
    w.show();

    return a.exec();
}
