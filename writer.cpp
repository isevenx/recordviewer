#include "writer.h"

Writer::Writer(QObject *parent) : QObject(parent)
{    
    matSize = Size(6,4);
    histSize = 25;
    ifStop = false;
    FPS = 25;
    emptyFrame = imread("abomination.jpg");
    Frame = emptyFrame;
    fout1.open("sMatrix.txt",fstream::out | fstream::trunc);
}

Writer::~Writer()
{
    fout1.close();
}

void Writer::startWrite(QTime atTime)
{
    QTime mTime = QTime::currentTime();
    QTimer::singleShot(mTime.msecsTo(atTime),this,SLOT(writeFrame()));
}

void Writer::writeFrame()
{
    if(ifStop)
        return;

    wTime.start();

    Mat matrix;
    QString tString;
    int hashtime = 0;

    if(Frame.type() != CV_8UC1)
        matrix = parseMat(Frame);
    tString = hashMat(matrix);

    imshow("Matrix Viewer",matrix);
    Frame = emptyFrame;
    fout1 << tString.toStdString().c_str() << std::endl;

    hashtime = wTime.elapsed();
    //qDebug() << hashtime;

    QTimer::singleShot((1000/FPS),this,SLOT(writeFrame()));
}

void Writer::changeFrame(Mat frame)
{
    Frame = frame;
}

Mat Writer::parseMat(Mat frame)
{
    Mat grayFrame (frame.size(), CV_8UC1);
    cv::cvtColor(frame,grayFrame,CV_BGR2GRAY);
    cv::resize(grayFrame,grayFrame,matSize);
    return grayFrame;
}

QString Writer::hashMat(Mat frame)
{
    Mat tMatrix = frame;
    string scValues;
    matValues matV;
    matV.x = tMatrix.cols;
    matV.y = tMatrix.rows;
    matV.values = new unsigned char[matV.x*matV.y];
    int j = -1;
    for(int y = 0;y < tMatrix.rows; y++){
        for(int x = 0; x < tMatrix.cols; x++){
            ++j;
            Scalar tValue = tMatrix.at<uchar>(Point(x, y));
            matV.values[j] = tValue.val[0];
            scValues.append(QString::number(tValue.val[0]).toStdString());
            scValues.append(",");
        }
    }
    scValues.erase(scValues.length()-1);

    return QString(QString::number(tMatrix.cols)+ "," +QString::number(tMatrix.rows) + "{"\
                   + scValues.c_str() + "}");
}

QString Writer::hashHist(Mat frame)
{
    string histValues;
    QList<long> tempHist;
    long tempValue = 0;
    Mat src;
    cvtColor(frame,src,CV_BGR2GRAY);
    uchar * src_p, * src_end;
    long histogram[256] = {0};
    for( src_p = src.data, src_end = src.data + src.rows * src.cols;
         src_p != src_end; ++src_p)
    {
        histogram[*src_p]++;
    }
    for(int i = 0; i < 256; i++){
        tempValue += histogram[i];
        if(i%int(255/histSize) == 0 && i<250 && i != 0){
            tempHist.push_back(tempValue);
            histValues.append(QString::number(tempValue).toStdString());
            histValues.append(",");
            tempValue = 0;
        }
    }
    histValues.append(QString::number(tempValue).toStdString());

    return QString(QString::number(histSize)+ "{" +histValues.c_str()+ "}");
}

