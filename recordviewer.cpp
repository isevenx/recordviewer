#include "recordviewer.h"
#include "ui_recordviewer.h"

recordViewer::recordViewer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::recordViewer)
{
    ui->setupUi(this);

    grabThread = new QThread();
    grabber = new Grabber();

    writeThread = new QThread();
    writer = new Writer();

    grabber->moveToThread(grabThread);
    grabThread->start();

    writer->moveToThread(writeThread);
    writeThread->start();

    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(startRecord()));

    connect(ui->pushButton_2,SIGNAL(clicked()),this,SLOT(stopRecord()));

    connect(grabber,SIGNAL(sendFrame(Mat)),this,SLOT(getFrame(Mat)));
    connect(this,SIGNAL(changeFrame(Mat)),writer,SLOT(changeFrame(Mat)));
    //connect(writeThread,SIGNAL(started()),writer,SLOT(startWrite()));
}

recordViewer::~recordViewer()
{

}

void recordViewer::closeEvent(QCloseEvent*)
{    
    delete grabber;
    delete ui;
    writer->ifStop = true;
    QThread::msleep(100);
    writeThread->quit();
    delete writer;

    destroyAllWindows();
    this->close();
}

void recordViewer::startRecord()
{
    grabber->ifStop = false;
    writer->ifStop = false;
    ui->pushButton->setEnabled(false);
    QTime time,atTime;
    time = time.currentTime();
    atTime.setHMS(time.hour(),time.minute(),30,0);
    grabber->startGrab(atTime);
    writer->startWrite(atTime);
}

void recordViewer::stopRecord()
{
    grabber->ifStop = true;
    writer->ifStop = true;
    ui->pushButton->setEnabled(true);
}

void recordViewer::getFrame(Mat frame)
{
    imshow("Video Viewer",frame);
    emit changeFrame(frame.clone());
}
