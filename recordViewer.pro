#-------------------------------------------------
#
# Project created by QtCreator 2015-03-11T11:18:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = recordViewer
TEMPLATE = app


SOURCES += main.cpp\
        recordviewer.cpp \
    grabber.cpp \
    writer.cpp

LIBS += -L/usr/local/lib
LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core

HEADERS  += recordviewer.h \
    grabber.h \
    writer.h

FORMS    += recordviewer.ui
