#ifndef WRITER_H
#define WRITER_H

#include <QObject>
#include <QThread>
#include <QTime>
#include <QTimer>
#include <QDebug>

#include <fstream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class Writer : public QObject
{
    Q_OBJECT
public:
    explicit Writer(QObject *parent = 0);
    ~Writer();

    Mat Frame, emptyFrame;
    int delay, FPS;
    bool ifStop;

signals:

public slots:
    void startWrite(QTime atTime);
    void writeFrame();
    void changeFrame(Mat frame);

private:
    struct matValues{
        int x;
        int y;
        unsigned char *values;
    };
    QString hashMat(Mat frame);
    QString hashHist(Mat frame);
    Mat parseMat(Mat frame);
    Size matSize;
    int histSize;

    QTime wTime,bench;
    fstream fout1, fout2;
};

#endif // WRITER_H
